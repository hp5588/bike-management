/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.api.model.Customer;
import com.hp5588.bikemanagement.api.model.Reservation;
import com.hp5588.bikemanagement.api.model.Transaction;
import com.hp5588.bikemanagement.api.model.remodel.BikeProfile;
import com.hp5588.bikemanagement.api.model.remodel.OverviewProfile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class WebApi {
    private static WebApi instance;

    private String LOG_TAG = "WebApi";
    private String urlString;
    private ObjectMapper mMapper;
    private SimpleDateFormat formatter;

    private String bikesUrlPrefix = "bikes/";

    enum WebController {
        bikes,
        customers,
        transactions,
        reservations
    }
    enum BikeAction {
        overview,
        available,
        profile,
        records,
        rent,
        Return,
        repair,
        reserve,
        return_list_today,
        prepare_list_today,
        list
    }
    enum CustomerApi{
        available,
        info
    }
    enum TransactionApi{

    }
    enum ReservationApi {
        today,
        monthly
    }

    public WebApi(String url){
        this.urlString = url;
        this.mMapper= new ObjectMapper();

        //2018-07-13 00:00:00
        formatter = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss", Locale.GERMANY);
    }

    public static WebApi getInstance() {
        if (instance==null){
            instance = new WebApi("");
        }
        return instance;
    }

    //bike
    public Bike[] bike_available(int category){

        Map<String,String> parameters = new HashMap<>();
        parameters.put("category",String.valueOf(category));
        Bike[] bikes = request(WebController.bikes, BikeAction.available.ordinal(),parameters, Bike[].class);
        return bikes;

    }

    public Bike[] bike_list(){

        return request(WebController.bikes, BikeAction.list.ordinal(),null, Bike[].class);
    }

    public BikeProfile bike_profile(String bikeIdNumber){

        BikeProfile profile = new BikeProfile();

        Map<String,String> parameters = new HashMap<>();
        parameters.put("bike_id_number", bikeIdNumber);

        Object[] bikes = request(WebController.bikes, BikeAction.profile.ordinal(),parameters, Object[].class);
        if (bikes==null)
            return null;

        profile.setBike(mMapper.convertValue(bikes[0],Bike.class));
        profile.setCustomer(mMapper.convertValue(bikes[1], Customer.class));
        profile.setTransaction(mMapper.convertValue(bikes[2], Transaction.class));

        return profile;

    }

    public Transaction[] bike_records(String bikeIdNumber){

        Map<String,String> parameters = new HashMap<>();
        parameters.put("bike_id_number", bikeIdNumber);

        return request(WebController.bikes, BikeAction.records.ordinal(),parameters, Transaction[].class);
    }



    public void bike_rent(Bike bike, int customerId){
        Map<String,String> parameters = new HashMap<>();
        parameters.put("bike_id_number", bike.getBike_id_number());
        parameters.put("customer_id", String.valueOf(customerId));
        parameters.put("rent_date", formatter.format(bike.getRent_date()));
        parameters.put("return_date", formatter.format(bike.getReturn_date()));

        request(WebController.bikes, BikeAction.rent.ordinal(),parameters, null);
    }

    public void bike_return(Bike bike, int customerId){
        Map<String,String> parameters = new HashMap<>();
        parameters.put("bike_id_number", bike.getBike_id_number());
        parameters.put("customer_id", String.valueOf(customerId));

        request(WebController.bikes, BikeAction.Return.ordinal(),parameters, null);
    }

    public void bike_repair(Bike bike){
        Map<String,String> parameters = new HashMap<>();
        parameters.put("bike_id_number", bike.getBike_id_number());
        request(WebController.bikes, BikeAction.repair.ordinal(),parameters, null);
    }

    public Bike[] bikes_return_list_today(){
        LinkedHashMap[] objects = request(WebController.bikes, BikeAction.return_list_today.ordinal(),null, LinkedHashMap[].class);
        if (objects==null)
            return null;

        ArrayList<Bike> bikes = new ArrayList<>();
        for (LinkedHashMap object :
                objects) {
            Bike bike = mMapper.convertValue(object,Bike.class);
            bike.setCustomer(mMapper.convertValue(object,Customer.class));
            bike.setTransaction(mMapper.convertValue(object,Transaction.class));
            bikes.add(bike);
        }

        return bikes.toArray(new Bike[0]);
    }

    public OverviewProfile bike_overview(){
        OverviewProfile []objects = request(WebController.bikes, BikeAction.overview.ordinal(),null, OverviewProfile[].class);
        if (objects==null)
            return null;

        return objects[0];
    }

    //customer

    //reservation
    public Reservation[] bike_prepare_list_today(){
        Map<String,String> parameters = new HashMap<>();
        LinkedHashMap[] objects = (LinkedHashMap[]) request(WebController.bikes, BikeAction.prepare_list_today.ordinal(),parameters, LinkedHashMap[].class);
        if (objects == null)
            return null;


        ArrayList<Reservation> reservations = new ArrayList<>();
        for (LinkedHashMap object: objects) {
            Reservation reservation = mMapper.convertValue(object.get(WebController.reservations.name()),Reservation.class);
            reservation.setCustomer(mMapper.convertValue(object.get(WebController.customers.name()),Customer.class));
            reservation.setBike(mMapper.convertValue(object.get(WebController.bikes.name()),Bike.class));

            reservations.add(reservation);
        }


        return reservations.toArray(new Reservation[reservations.size()]);
    }

    public Reservation[] reservation_monthly(){

        String responseString = requestString(WebController.reservations, ReservationApi.monthly.ordinal(),null);

        if (responseString == null)
            return null;

        //manually parse it
        Reservation []reservations = null;
        Bike[] bikes = null;
        Customer[] customers = null;
        try {
            reservations = mMapper.readValue(responseString,Reservation[].class);
            bikes = mMapper.readValue(responseString,Bike[].class);
            customers = mMapper.readValue(responseString,Customer[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //put all into reservation

        if (reservations != null && bikes != null && customers != null) {
            for (int i = 0; i < reservations.length; i++) {
                Reservation reservation = reservations[i];
                reservation.setBike(bikes[i]);
                reservation.setCustomer(customers[i]);
//                reservations[i] = reservation;
            }
        }



        return reservations;
    }



        private <T> T request(WebController webController, int api, Map<String, String> parameters, Class<T> tClass ) {
            String respondString = requestString(webController, api, parameters);
            T result = null;
            if (respondString ==null)
                return null;

            try {
                result =  mMapper.readValue(respondString, tClass);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }


        private String requestString(WebController webController, int api, Map<String, String> parameters) {
        String requestUrlString = urlString + webController.name() + "/";
        String respondString;
        switch (webController){
            case bikes:
                BikeAction bikeAction =  BikeAction.values()[api];
//                requestUrlString = requestUrlString + webController.name() + "/" + bikeAction;
                requestUrlString += bikeAction;
                break;
            case customers:
                CustomerApi customerApi =  CustomerApi.values()[api];
                requestUrlString += customerApi;
                break;
            case transactions:
                TransactionApi transactionApi =  TransactionApi.values()[api];
                requestUrlString += transactionApi;
                break;
            case reservations:
                ReservationApi reservationApi = ReservationApi.values()[api];
                requestUrlString  += reservationApi;
                break;
        }
        respondString = executeRequest(requestUrlString, parameters);
        if(respondString.length() == 0){
            return null;
        }

        return respondString;
    }





    private String executeRequest(final String urlString, final Map<String, String> parameters) {

        final StringBuilder response = new StringBuilder();
        final String result[] = new String[1];

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection conn = null;
                String finalUrlString = urlString;
                try {
                    finalUrlString += "?";

                    if (parameters != null) {
                        for (Map.Entry<String, String> parameter :
                                parameters.entrySet()) {
                            finalUrlString += (parameter.getKey() + "=" + parameter.getValue()) + "&";
//                            if (!((TreeMap<String, String>)parameters).lastKey().equals(parameter.getKey()))
//                                finalUrlString += "&"
                        }

                    }


                    conn = (HttpURLConnection) new URL(finalUrlString).openConnection();
                    conn.setDoOutput(false);
                    conn.setDoInput(true);
                    conn.setUseCaches(false);
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");


                    // handle the response
                    int status = conn.getResponseCode();
                    if (status != 200) {
                        throw new IOException("Post failed with error code " + status);
                    } else {
                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String inputLine;
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                    result[0] = response.toString();
                }

            }

        });

        try {
            thread.start();
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result[0];

    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }


}
