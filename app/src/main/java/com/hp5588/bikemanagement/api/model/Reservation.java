/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hp5588.bikemanagement.api.model.remodel.Category;

import java.sql.Timestamp;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Reservation {

    //model for linked object
    private Bike bike;
    private Customer customer;

    //native data
    private Timestamp reservation_rent_date;
    private Timestamp reservation_return_date;
    private int id;
    private int customer_id;
    private int bike_id;
    private Timestamp created_at;
    private Timestamp updated_at;


    public static Category[] groupByCategory(Reservation[] reservations){
        Category[] categories = new Category[Bike.Category.values().length];
//        List<Category> categories = new ArrayList<>(Bike.Category.values().length);

        for (Reservation reservation: reservations) {
            int categoryIndex = reservation.getBike().getCategory().ordinal();
            Category category = categories[categoryIndex];
            if (category == null)
            {
                category = new Category();
            }

            category.addReservation(reservation);

            categories[categoryIndex] = category;
        }

//        return categories.toArray(new Category[0]);
        return categories;
    }


    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getBike_id() {
        return bike_id;
    }

    public void setBike_id(int bike_id) {
        this.bike_id = bike_id;
    }


    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Timestamp getReservation_rent_date() {
        return reservation_rent_date;
    }

    public void setReservation_rent_date(Timestamp reservation_rent_date) {
        this.reservation_rent_date = reservation_rent_date;
    }

    public Timestamp getReservation_return_date() {
        return reservation_return_date;
    }

    public void setReservation_return_date(Timestamp reservation_return_date) {
        this.reservation_return_date = reservation_return_date;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }
}
