/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.api.model.remodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.LinkedHashMap;
@JsonIgnoreProperties(ignoreUnknown = true)
public class OverviewProfile {

    private Integer insideCount;
    private Integer outsideCount;
    private Integer repairCount;
    private LinkedHashMap<Integer, LinkedHashMap<Integer,Integer>> availableCount;

    public Integer getRepairCount() {
        return repairCount;
    }

    public void setRepairCount(Integer repairCount) {
        this.repairCount = repairCount;
    }

    public LinkedHashMap<Integer, LinkedHashMap<Integer, Integer>> getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(LinkedHashMap<Integer, LinkedHashMap<Integer, Integer>> availableCount) {
        this.availableCount = availableCount;
    }

    public Integer getInsideCount() {
        return insideCount;
    }

    public void setInsideCount(Integer insideCount) {
        this.insideCount = insideCount;
    }

    public Integer getOutsideCount() {
        return outsideCount;
    }

    public void setOutsideCount(Integer outsideCount) {
        this.outsideCount = outsideCount;
    }









}
