/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.bike;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.activities.MainActivity;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.api.model.Customer;
import com.hp5588.bikemanagement.api.model.Transaction;
import com.hp5588.bikemanagement.api.model.remodel.BikeProfile;
import com.hp5588.bikemanagement.utility.DefaultAsyncTask;
import com.ncapdevi.fragnav.FragNavController;
import com.roger.catloadinglibrary.CatLoadingView;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class BikeProfileFragment extends Fragment {

    private CatLoadingView loadingView;
    private String bikeIdNumber;

    private BikeProfile bikeProfile;
    private Bike bike;
    private Transaction transaction ;
    private Customer customer;

    private LinkedHashMap<Transaction.Status, LinkedHashMap<Integer,Boolean>> optionsEnableSettings;


    //UI component
    private TextView bikeIdNumberTextView;
    private TextView statusTextView;
    private TextView typeTextView;
    private TextView renterTextView;
    private TextView phoneTextView;
    private TextView rentDateTextView;
    private TextView returnDateTextView;
    private Button callButton;
    private View callingView;

    private Snackbar snackbar;
    private FragNavController fragNavController;
    private WebApi api;

    public BikeProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initOptionsSetting();
        api = WebApi.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        MainActivity.getMainActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        MainActivity.getMainActivity().getSupportActionBar().setTitle("Test");

        return inflater.inflate(R.layout.fragment_bike_profile, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        fragNavController = MainActivity.getMainActivity().getmFragNavController();

        //find UI component
        bikeIdNumberTextView = getView().findViewById(R.id.bike_profile_title);
        statusTextView = getView().findViewById(R.id.bike_profile_status_textview);
        typeTextView = getView().findViewById(R.id.bike_profile_type_textview);
        renterTextView = getView().findViewById(R.id.bike_profile_renter_textview);
        phoneTextView = getView().findViewById(R.id.bike_profile_phone_textview);
        callButton = getView().findViewById(R.id.bike_profile_call_button);
        rentDateTextView = getView().findViewById(R.id.bike_profile_rentdate_textview);
        returnDateTextView = getView().findViewById(R.id.bike_profile_returndate_textview);
        callingView = getView().findViewById(R.id.bike_profile_calling_view);


        loadingView = MainActivity.getMainActivity().getLoadingView();

        snackbar = Snackbar.make(MainActivity.getMainActivity().findViewById(R.id.inner_container), R.string.snack_warn_bike_not_exist, Snackbar.LENGTH_LONG);


        //init UI state
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", customer.getPhone_number(), null));
                startActivity(intent);
            }
        });


        ProfileUpdateTask profileUpdateTask = new ProfileUpdateTask();
        profileUpdateTask.execute();


        Button actionButton = getView().findViewById(R.id.bike_profile_action_button);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPopupMenus(view);
            }
        });


    }

    private void createPopupMenus(View anchorView){
        PopupMenu popup = new PopupMenu(getActivity(), anchorView);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.operation_option, popup.getMenu());

        //set popup menu visibility
        LinkedHashMap<Integer, Boolean> settings = optionsEnableSettings.get(transaction.getStatus());

        for (LinkedHashMap.Entry<Integer,Boolean> setting :
             settings.entrySet()) {
            popup.getMenu().findItem(setting.getKey()).setVisible(setting.getValue());
        }


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.act_options_rent:{
                        //TODO update profile as well
                        fragNavController.pushFragment(ChecklistFragment.newInstance(ChecklistFragment.Direction.Rent, customer, bike));

                        break;
                    }
                    case R.id.act_options_reserve:{
                        break;
                    }
                    case R.id.act_options_return:{

                        fragNavController.pushFragment(ChecklistFragment.newInstance(ChecklistFragment.Direction.Return, customer, bike));

                        break;
                    }
                    case R.id.act_options_repair:{
                        api.bike_repair(bike);
                        ProfileUpdateTask task = new ProfileUpdateTask();
                        task.execute();
                    }

                }
                return false;
            }
        });

        popup.show();
    }

    public String getBikeIdNumber() {
        return bikeIdNumber;
    }

    public void setBikeIdNumber(String bikeIdNumber) {
        this.bikeIdNumber = bikeIdNumber;
    }



    @SuppressLint("StaticFieldLeak")
    private class ProfileUpdateTask extends DefaultAsyncTask {


        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(Object o) {
            if(bikeProfile == null)
            {
                MainActivity.getMainActivity().getmFragNavController().popFragment();
                loadingView.dismiss();
                snackbar.show();
                return;
            }

            //get objects
            bike = bikeProfile.getBike();
            transaction = bikeProfile.getTransaction();
            customer = bikeProfile.getCustomer();

            //update UI here in main thread
            bikeIdNumberTextView.setText(bike.getBike_id_number());
            typeTextView.setText(bike.getCategory().name());
            renterTextView.setText(customer.getFirst_name() + " " + customer.getLast_name());
            statusTextView.setText(transaction.getStatus().name());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.GERMANY);
            if (transaction.getRent_date() != null)
                rentDateTextView.setText(dateFormat.format(transaction.getRent_date()));
            else
                rentDateTextView.setText(R.string.empty_data_field);

            if (transaction.getReturn_date() != null)
                returnDateTextView.setText(dateFormat.format(transaction.getReturn_date()));
            else
                returnDateTextView.setText(R.string.empty_data_field);

            if (customer.getPhone_number() == null)
                callingView.setVisibility(View.GONE);
            else
                phoneTextView.setText(customer.getPhone_number());


            loadingView.dismiss();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            WebApi api = WebApi.getInstance();
            bikeProfile = api.bike_profile(bikeIdNumber);
//            bike = api.bike_profile(bikeIdNumber);
//            transactions = api.bike_records(bikeIdNumber);
            return null;
        }
    };


    private void initOptionsSetting(){

        LinkedHashMap<Integer,Boolean> insideSetting = new LinkedHashMap<>();
        LinkedHashMap<Integer,Boolean> outsideSetting = new LinkedHashMap<>();
        LinkedHashMap<Integer,Boolean> repairSetting = new LinkedHashMap<>();

        insideSetting.put(R.id.act_options_add,false);
        insideSetting.put(R.id.act_options_remove,true);
        insideSetting.put(R.id.act_options_repair,true);
        insideSetting.put(R.id.act_options_reserve,true);
        insideSetting.put(R.id.act_options_rent,true);
        insideSetting.put(R.id.act_options_return,false);

        outsideSetting.put(R.id.act_options_add,false);
        outsideSetting.put(R.id.act_options_remove,false);
        outsideSetting.put(R.id.act_options_repair,true);
        outsideSetting.put(R.id.act_options_reserve,true);
        outsideSetting.put(R.id.act_options_rent,false);
        outsideSetting.put(R.id.act_options_return,true);

        repairSetting.put(R.id.act_options_add,false);
        repairSetting.put(R.id.act_options_remove,true);
        repairSetting.put(R.id.act_options_repair,false);
        repairSetting.put(R.id.act_options_reserve,true);
        repairSetting.put(R.id.act_options_rent,false);
        repairSetting.put(R.id.act_options_return,true);

        optionsEnableSettings = new LinkedHashMap<>();
        optionsEnableSettings.put(Transaction.Status.In, insideSetting);
        optionsEnableSettings.put(Transaction.Status.Out, outsideSetting);
        optionsEnableSettings.put(Transaction.Status.Repair, repairSetting);

    }

}
