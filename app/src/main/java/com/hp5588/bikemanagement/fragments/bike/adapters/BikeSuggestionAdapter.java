/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.bike.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.fragments.bike.SuggestionHolder;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;

public class BikeSuggestionAdapter extends SuggestionsAdapter<Bike,SuggestionHolder> {

    private OnItemViewClickListener suggestionsClickListener;

    public BikeSuggestionAdapter(LayoutInflater inflater) {
        super(inflater);
    }

    @Override
    public void onBindSuggestionHolder(Bike suggestion, final SuggestionHolder holder, final int position) {
        holder.getTitle().setText(suggestion.getBike_id_number());
        holder.getSubtitle().setText(suggestion.getCategory().name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (suggestionsClickListener!=null){
                    suggestionsClickListener.OnItemClickListener(position, holder.itemView);
                }
            }
        });

    }

    @Override
    public int getSingleViewHeight() {
        return 60;
    }


    @NonNull
    @Override
    public SuggestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = getLayoutInflater().inflate(R.layout.template_suggestion_row, parent, false);

        return new SuggestionHolder(view);
    }

    public OnItemViewClickListener getSuggestionsClickListener() {
        return suggestionsClickListener;
    }

    public void setSuggestionsClickListener(OnItemViewClickListener suggestionsClickListener) {
        this.suggestionsClickListener = suggestionsClickListener;
    }
}
