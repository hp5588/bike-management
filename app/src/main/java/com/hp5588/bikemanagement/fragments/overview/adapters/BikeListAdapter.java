/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.api.model.Customer;

import java.util.List;

public class BikeListAdapter extends ArrayAdapter<Bike> {
    public BikeListAdapter(@NonNull Context context, int resource, @NonNull List<Bike> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.template_bike_customer_row,parent,false);
        }
        TextView customerNameTextView = convertView.findViewById(R.id.preparelist_detail_customer_name);
        TextView customerPhoneTextView = convertView.findViewById(R.id.preparelist_detail_customer_phonenumber);
        TextView bikeCategoryTextView = convertView.findViewById(R.id.preparelist_detail_bike_category);
        TextView bikeIdNumberTextView = convertView.findViewById(R.id.preparelist_detail_bike_id_number);

        Bike bike = getItem(position);
        Customer customer = bike.getCustomer();
        customerNameTextView.setText(customer.getFirst_name() + " " + customer.getLast_name());
        customerPhoneTextView.setText(customer.getPhone_number());
        bikeCategoryTextView.setText(bike.getCategory().name());
        bikeIdNumberTextView.setText(bike.getBike_id_number());


        return convertView;
    }
}
