/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.returm;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.fragments.overview.adapters.BikeListAdapter;
import com.hp5588.bikemanagement.utility.DefaultAsyncTask;
import com.hp5588.bikemanagement.utility.Helper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReturnListFragment extends Fragment {

    private ArrayList<Bike> bikes = new ArrayList<>();
    private BikeListAdapter listAdapter;

    public ReturnListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_return_list, container, false);

        final ListView listView = view.findViewById(R.id.return_listview);
        listAdapter = new BikeListAdapter(getContext(),R.layout.template_bike_customer_row,bikes);
        listView.setAdapter(listAdapter);


        class UpdateTask extends DefaultAsyncTask{

            @Override
            protected void onPostExecute(Object o) {
                //update UI
                listAdapter.clear();
                listAdapter.addAll(bikes);
                listAdapter.notifyDataSetChanged();
                Helper.getInstance().dismissLoadingView();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                bikes = new ArrayList<>(Arrays.asList(WebApi.getInstance().bikes_return_list_today()));
                return null;
            }
        }

        UpdateTask task = new UpdateTask();
        task.execute();


        return view;
    }

}
