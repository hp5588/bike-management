/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.prepare;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.activities.MainActivity;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.Reservation;
import com.hp5588.bikemanagement.api.model.remodel.Category;
import com.hp5588.bikemanagement.fragments.overview.adapters.CategoryListAdapter;
import com.hp5588.bikemanagement.utility.DefaultAsyncTask;
import com.hp5588.bikemanagement.utility.Helper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrepareListFragment extends Fragment {

    private CategoryListAdapter adapter;
    private Category[] categories = new Category[0];

    public PrepareListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_prepare_list, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        ArrayList<Category> categoryArrayList = new ArrayList<Category>(Arrays.asList(categories));
        adapter = new CategoryListAdapter(this.getContext(),R.layout.template_bike_category_row, categoryArrayList);

        ListView listView = getView().findViewById(R.id.preparelist_listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Category category = categories[i];
                if (category != null){
                    DetailPrepareListFragment detailPrepareListFragment = new DetailPrepareListFragment();
                    detailPrepareListFragment.setCategory(category);

                    MainActivity.getMainActivity().getmFragNavController().pushFragment(detailPrepareListFragment);
                }
            }
        });


        class UpdateTask extends DefaultAsyncTask{
            private Reservation []reservations;

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                //update UI
                adapter.clear();
                adapter.addAll(categories);
                adapter.notifyDataSetChanged();

                Helper.getInstance().dismissLoadingView();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                reservations = WebApi.getInstance().bike_prepare_list_today();
                categories = Reservation.groupByCategory(reservations);
                return null;
            }
        }


        UpdateTask task = new UpdateTask();
        task.execute();


    }
}
