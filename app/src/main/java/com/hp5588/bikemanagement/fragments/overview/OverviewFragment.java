/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.activities.MainActivity;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.remodel.OverviewProfile;
import com.hp5588.bikemanagement.fragments.overview.adapters.CategoryAdapter;
import com.hp5588.bikemanagement.fragments.overview.prepare.PrepareListFragment;
import com.hp5588.bikemanagement.fragments.overview.reservation.ReservationFragment;
import com.hp5588.bikemanagement.fragments.overview.returm.ReturnListFragment;
import com.hp5588.bikemanagement.utility.Helper;
import com.ncapdevi.fragnav.FragNavController;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OverviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverviewFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private CategoryAdapter adapter;
    private ArrayList<LinkedHashMap<Integer,Integer>> maps = new ArrayList<>();

    private FragNavController fragNavController;
    private WebApi api;

    private int taskCount = 0;

    public OverviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OverviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OverviewFragment newInstance(String param1, String param2) {
        OverviewFragment fragment = new OverviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //update profile
        api = WebApi.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_overview, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        fragNavController = MainActivity.getMainActivity().getmFragNavController();

        adapter = new CategoryAdapter(maps);
        RecyclerView recycleView = getView().findViewById(R.id.category_recycleview);
        recycleView.setAdapter(adapter);


        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);


        getView().findViewById(R.id.overview_prepare_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragNavController.pushFragment(new PrepareListFragment());
            }
        });
        getView().findViewById(R.id.overview_reservation_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragNavController.pushFragment(new ReservationFragment());
            }
        });
        getView().findViewById(R.id.overview_today_return_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragNavController.pushFragment(new ReturnListFragment());
            }
        });

        if (taskCount ==0){
            UpdateTask task = new UpdateTask();
            Object[] parameters = {this};
            task.execute(parameters);
        }

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class UpdateTask extends AsyncTask{
        List<LinearLayout> resultViews = new ArrayList<>();
        OverviewProfile overviewProfiles;

        @Override
        protected void onPostExecute(Object o) {

            //UI update
            ArrayList<LinkedHashMap<Integer, Integer>> items = new ArrayList<>(overviewProfiles.getAvailableCount().values());
            maps = items;
            adapter.setmDataSet(items);
            adapter.notifyDataSetChanged();

            ((TextView)getView().findViewById(R.id.overview_inside_count)).setText(String.valueOf(overviewProfiles.getInsideCount()));
            ((TextView)getView().findViewById(R.id.overview_outside_count)).setText(String.valueOf(overviewProfiles.getOutsideCount()));
            ((TextView)getView().findViewById(R.id.overview_repair_count)).setText(String.valueOf(overviewProfiles.getRepairCount()));

            Helper.getInstance().dismissLoadingView();
            taskCount --;
        }

        @Override
        protected void onPreExecute() {
            MainActivity.getMainActivity().getLoadingView().show(getFragmentManager(),null);
            taskCount++;

        }

        @Override
        protected Object doInBackground(Object[] objects) {
            overviewProfiles = api.bike_overview();
            return null;
        }
    }
}
