/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.reservation;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.Reservation;
import com.hp5588.bikemanagement.fragments.overview.adapters.DetailPrepareListAdapter;
import com.hp5588.bikemanagement.utility.DefaultAsyncTask;
import com.hp5588.bikemanagement.utility.Helper;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class ReservationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ReservationFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private List<Reservation> reservations = new ArrayList<>();
    private List<Reservation> selectedReservations = new ArrayList<>();

    private DetailPrepareListAdapter detailPrepareListAdapter;
    private CompactCalendarView compactCalendarView;

    private TextView monthLabelTextView;

    private boolean isInited = false;


    public ReservationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReservationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReservationFragment newInstance(String param1, String param2) {
        ReservationFragment fragment = new ReservationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        class UpdateTask extends DefaultAsyncTask{
            @Override
            protected void onPostExecute(Object o) {
                //update UI
                //TODO here assume today is selected

                addEventsToCalendar();


                Helper.getInstance().dismissLoadingView();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                //grab
                reservations = new ArrayList<>(Arrays.asList(WebApi.getInstance().reservation_monthly()));
                return null;
            }
        }

        UpdateTask task = new UpdateTask();
        task.execute();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reservation, container, false);


        //UI
        monthLabelTextView = view.findViewById(R.id.reservation_month_label);
        updateMonthLabel(Calendar.getInstance().getTime());

        //list view
        detailPrepareListAdapter = new DetailPrepareListAdapter(getContext(), R.layout.template_bike_customer_row, selectedReservations);
        final ListView listView = view.findViewById(R.id.reservation_listview);
        listView.setAdapter(detailPrepareListAdapter);

        //calender view
        compactCalendarView = view.findViewById(R.id.compactcalendar_view);
        compactCalendarView.setFirstDayOfWeek(Calendar.SUNDAY);
        compactCalendarView.setLocale(TimeZone.getTimeZone("DE"), Locale.GERMANY);
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendarView.getEvents(dateClicked);

                //update list view
                detailPrepareListAdapter.clear();
                selectedReservations = new ArrayList<>(Arrays.asList(getReservationOnDate(new Timestamp(dateClicked.getTime()))));
                detailPrepareListAdapter.addAll(selectedReservations);
                detailPrepareListAdapter.notifyDataSetChanged();

//                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                updateMonthLabel(firstDayOfNewMonth);
//                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
            }
        });

        addEventsToCalendar();


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();



    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void updateMonthLabel(Date date){
        Format formatter = new SimpleDateFormat("yyyy MMMM", Locale.GERMANY);
        monthLabelTextView.setText(formatter.format(date));
    }


    private void addEventsToCalendar(){
        for (Reservation reservation :
                reservations) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(reservation.getReservation_rent_date());
            compactCalendarView.addEvent(new Event(Color.WHITE, calendar.getTimeInMillis()));
        }
    }
    private Reservation[] getReservationOnDate (Timestamp timestamp){
        Calendar referenceCalendar = Calendar.getInstance();
        referenceCalendar.setTime(timestamp);
        Calendar targetCalendar = Calendar.getInstance();

        ArrayList<Reservation> reservations_temp = new ArrayList<>();

        for (Reservation reservation : reservations) {
            targetCalendar.setTime(reservation.getReservation_rent_date());
            if (referenceCalendar.get(Calendar.DAY_OF_YEAR)==targetCalendar.get(Calendar.DAY_OF_YEAR)){
                reservations_temp.add(reservation);
            }
        }

        return reservations_temp.toArray(new Reservation[0]);
    }





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
