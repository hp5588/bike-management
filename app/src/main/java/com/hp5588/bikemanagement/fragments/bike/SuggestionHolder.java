/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.bike;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;

public class SuggestionHolder extends RecyclerView.ViewHolder {
    protected TextView title;
    protected TextView subtitle;

    public SuggestionHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.suggestion_title);
        subtitle = (TextView) itemView.findViewById(R.id.suggestion_subtitle);
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getSubtitle() {
        return subtitle;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public void setSubtitle(TextView subtitle) {
        this.subtitle = subtitle;
    }
}
