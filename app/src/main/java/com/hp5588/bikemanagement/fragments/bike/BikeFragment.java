/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.bike;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.activities.MainActivity;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.fragments.bike.adapters.BikeSuggestionAdapter;
import com.hp5588.bikemanagement.utility.DefaultAsyncTask;
import com.hp5588.bikemanagement.utility.Helper;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.ncapdevi.fragnav.FragNavController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BikeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class BikeFragment extends Fragment implements MaterialSearchBar.OnSearchActionListener{

    private OnFragmentInteractionListener mListener;
    private SurfaceView scannerSurfaceView;

    //searching components
    private MaterialSearchBar searchBar;
    private BikeSuggestionAdapter suggestionAdapter;
    private List<Bike> suggestionItems = new ArrayList<>();
    private List<Bike> allBikes = new ArrayList<>();
    private List<Bike> lastSuggestions = new ArrayList<>();

    private QREader qrEader;

    private WebApi api;
    private String LOG_STRING = "BikeFragment";
    private FragNavController fragNavController;

    private AppCompatActivity mainActivity;

    public BikeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_bike, container, false);

        api = WebApi.getInstance();
        mainActivity = MainActivity.getMainActivity();

        setHasOptionsMenu(true);
        mainActivity.getSupportActionBar().hide();

        searchBar = view.findViewById(R.id.searchBar);
        searchBar.setHint("search with bike number");
        searchBar.setSpeechMode(false);
        searchBar.setOnSearchActionListener(this);
        searchBar.setCardViewElevation(10);
        searchBar.setLastSuggestions(lastSuggestions);
        searchBar.enableSearch();
        EditText editText = searchBar.getRootView().findViewById(R.id.mt_editText);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        suggestionAdapter = new BikeSuggestionAdapter(getLayoutInflater());
        suggestionAdapter.setSuggestionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                Bike selectedBike = suggestionItems.get(position);
                lastSuggestions.add(selectedBike);
                BikeProfileFragment fragment = new BikeProfileFragment();
                fragment.setBikeIdNumber(selectedBike.getBike_id_number());
                fragNavController.pushFragment(fragment);
            }

            @Override
            public void OnItemDeleteListener(int position, View v) {

            }
        });
        searchBar.setCustomSuggestionAdapter(suggestionAdapter);
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                SearchTask searchTask = new SearchTask();
                searchTask.inputCharSequence = charSequence;
                searchTask.execute();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;
    }




    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fragNavController = MainActivity.getMainActivity().getmFragNavController();

        scannerSurfaceView = getView().findViewById(R.id.bike_scanner_surfaceview);
        qrEader = new QREader.Builder(getContext(), scannerSurfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                if (data.length() != 11)
                    return;

                if(fragNavController.isRootFragment()){
                    BikeProfileFragment bikeProfileFragment = new BikeProfileFragment();
                    bikeProfileFragment.setBikeIdNumber(data);
                    fragNavController.pushFragment(bikeProfileFragment);
                    Log.d("QREader", "Value : " + data);
                }
            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(scannerSurfaceView.getHeight())
                .width(scannerSurfaceView.getWidth())
                .build();

        UpdateTask updateTask = new UpdateTask();
        updateTask.execute();
    }


    @Override
    public void onResume() {
        qrEader.initAndStart(scannerSurfaceView);
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mainActivity.getSupportActionBar().show();
        qrEader.releaseAndCleanup();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onSearchStateChanged(boolean enabled) {

    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }


    private Bike[] searchFilter(Bike[] bikes, String query){

        if (query.length() == 0 || query.length() >= Bike.ID_NUMBER_MAX_LENGTH)
            return bikes;
        if (bikes.length >= 10000)
            return bikes;

        int MAX_INDEX = Bike.ID_NUMBER_MAX_LENGTH;
        Integer count = 0;
        Map<Integer, Bike> matchResults = new LinkedHashMap<>();

        String []elements = query.split("-");

        for (Bike bike: bikes) {
            int bestIndex = MAX_INDEX;
            for (String element :
                    elements) {
                int index = bike.getBike_id_number().indexOf(element);
                if (index>=0)
                    if (index < bestIndex)
                        bestIndex = index;
            }

            if (bestIndex < MAX_INDEX) {
                //match found and save the best match
                //avoid map collision we do *1000 and plus count
                matchResults.put(bestIndex * 10000 + count, bike);
                count++;
            }
        }

        Map<Integer, Bike> result = new TreeMap<>(matchResults);

        return result.values().toArray(new Bike[0]);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class SearchTask extends AsyncTask{
        public CharSequence inputCharSequence = "";
        @Override
        protected Object doInBackground(Object[] objects) {

            Bike[]filteredBike = searchFilter(allBikes.toArray(new Bike[0]), inputCharSequence.toString());
            suggestionItems = new ArrayList<>( Arrays.asList(filteredBike));

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            suggestionAdapter.setSuggestions(suggestionItems);
            suggestionAdapter.notifyDataSetChanged();
        }
    }

    public class UpdateTask extends DefaultAsyncTask{
        @Override
        protected Object doInBackground(Object[] objects) {
            Bike[] bikes = api.bike_list();

            if (bikes != null) {
                allBikes = new ArrayList<>(Arrays.asList(bikes));
                suggestionItems = new ArrayList<>(allBikes);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {

            suggestionAdapter.setSuggestions(suggestionItems);
            suggestionAdapter.notifyDataSetChanged();


            Helper.getInstance().dismissLoadingView();
        }

    }
}