/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.bike;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.activities.MainActivity;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.api.model.Customer;
import com.hp5588.bikemanagement.utility.DefaultAsyncTask;
import com.hp5588.bikemanagement.utility.Helper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChecklistFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChecklistFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChecklistFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String TAG_FROM_PICKER = "FROM_PICKER";
    private String TAG_TO_PICKER = "TO_PICKER";

    private Date rentDate;
    private Date returnDate;

    //UI component
    private Button confirmButton;
    private TextView titleTextView;

    private Spinner daySpinner;
    private Spinner hourSpinner;
    private Spinner minutesSpinner;

    private View fromDateView;
    private View toDateView;
    private TextView fromDateTextView;
    private TextView toDateTextView;


    private OnFragmentInteractionListener mListener;
    private WebApi api;

    private Direction direction;
    private Customer customer;
    private Bike bike;


    public enum Direction{
        Return,
        Rent
    }

    public ChecklistFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ChecklistFragment newInstance(Direction direction, Customer customer, Bike bike) {
        ChecklistFragment fragment = new ChecklistFragment();
        Bundle args = new Bundle();
        fragment.direction = direction;
        fragment.customer = customer;
        fragment.bike = bike;
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        api = WebApi.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_checklist, container, false);

        //init UI component
        titleTextView = view.findViewById(R.id.checklist_title);
        daySpinner = view.findViewById(R.id.day_spinner);
        hourSpinner = view.findViewById(R.id.hour_spinner);
        minutesSpinner = view.findViewById(R.id.minute_spinner);
        fromDateTextView = view.findViewById(R.id.from_date_textview);
        toDateTextView = view.findViewById(R.id.to_date_textview);

        switch (direction){
            case Return:
                titleTextView.setText(getResources().getString(R.string.checklist_title_return));
                break;
            case Rent:
                titleTextView.setText(getResources().getString(R.string.checklist_title_rent));
                break;
        }


        AdapterView.OnItemSelectedListener selectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateDateView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        daySpinner.setOnItemSelectedListener(selectedListener);
        hourSpinner.setOnItemSelectedListener(selectedListener);
        minutesSpinner.setOnItemSelectedListener(selectedListener);



//
//        Calendar now = Calendar.getInstance();
//        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
//                this,
//                now.get(Calendar.YEAR),
//                now.get(Calendar.MONTH),
//                now.get(Calendar.DAY_OF_MONTH)
//        );

        if (direction == Direction.Rent) {
//            View fromDateView = view.findViewById(R.id.from_date_button);
//            View toDateView = view.findViewById(R.id.to_date_button);
//
//            fromDateView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    datePickerDialog.show(MainActivity.getMainActivity().getFragmentManager(), TAG_FROM_PICKER);
//                }
//            });
//            toDateView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    datePickerDialog.show(MainActivity.getMainActivity().getFragmentManager(), TAG_TO_PICKER);
//                }
//            });
        }else if (direction == Direction.Return){
            //hide data picker
            view.findViewById(R.id.date_picker_row).setVisibility(View.GONE);
        }



        TableLayout tableLayout =  view.findViewById(R.id.checklist_table);
        List<String> items = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.check_items)));
        for (String content :
                items) {
            TableRow tableRow = (TableRow) this.getLayoutInflater().inflate(R.layout.template_return_checklist_row, null);

            ((CheckBox)tableRow.findViewById(R.id.checklist_item_checkbox)).setText(content);
            tableLayout.addView(tableRow);
        }



        //set listener for confirmation button
        confirmButton = view.findViewById(R.id.return_confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                class ReturnActionTask extends DefaultAsyncTask {

                    @Override
                    protected void onPostExecute(Object o) {
                        MainActivity.getMainActivity().getmFragNavController().popFragment();
                        Helper.getInstance().dismissLoadingView();
                    }

                    @Override
                    protected Object doInBackground(Object[] objects) {
                        switch (direction){
                            case Return:
                                WebApi.getInstance().bike_return(bike, customer.getId());
                                break;
                            case Rent:
                                if (rentDate != null && returnDate != null){
                                    bike.setRent_date(rentDate);
                                    bike.setReturn_date(returnDate);
                                    //TODO assume customer 2 rented
//                                    WebApi.getInstance().bike_rent(bike, customer.getId());
                                    WebApi.getInstance().bike_rent(bike, 2);
                                }
                                break;
                        }
                        return null;
                    }
                }

                ReturnActionTask returnActionTask = new ReturnActionTask();
                returnActionTask.execute();

            }
        });


        // Inflate the layout for this fragment
        return view ;


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }



    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        //init status
        setDisplayAndSaveDate(TAG_FROM_PICKER, Calendar.getInstance().getTime());
        setDisplayAndSaveDate(TAG_TO_PICKER, Calendar.getInstance().getTime());

    }

    @Override
    public void onResume() {
        super.onResume();


    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String tag = view.getTag();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,monthOfYear,dayOfMonth);

        setDisplayAndSaveDate(tag, calendar.getTime());
    }

    private void updateDateView(){
        String selectedDay = getResources().getStringArray(R.array.days)[daySpinner.getSelectedItemPosition()];
        String selectedHour = getResources().getStringArray(R.array.hours)[hourSpinner.getSelectedItemPosition()];
        String selectedMinute = getResources().getStringArray(R.array.minutes)[minutesSpinner.getSelectedItemPosition()];

        Calendar calendar = Calendar.getInstance();
        setDisplayAndSaveDate(TAG_FROM_PICKER, calendar.getTime());
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.add(Calendar.MINUTE, Integer.parseInt(selectedMinute));
        calendar.add(Calendar.HOUR, Integer.parseInt(selectedHour));
        calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(selectedDay));

        setDisplayAndSaveDate(TAG_TO_PICKER, calendar.getTime());

    }
    private void setDisplayAndSaveDate(String tag, Date date){
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm",Locale.GERMANY);

        if (tag.equals(TAG_FROM_PICKER)){
            rentDate = date;
            fromDateTextView.setText(dateFormatter.format(rentDate));
        }else if(tag.equals(TAG_TO_PICKER)){
            returnDate = date;
            toDateTextView.setText(dateFormatter.format(returnDate));
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }





}
