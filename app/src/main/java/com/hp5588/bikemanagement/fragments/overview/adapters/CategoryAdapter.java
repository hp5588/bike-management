/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.model.Bike;

import java.util.LinkedHashMap;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.RowViewHolder> {

    public class RowViewHolder extends RecyclerView.ViewHolder{
        private View rowView;
        public TextView availableNumTextView ;
        public TextView totalNumTextView;
        public TextView bikeTypeTextView;


        public RowViewHolder(View itemView) {
            super(itemView);
            this.rowView = itemView;

             availableNumTextView = rowView.findViewById(R.id.available_num_text);
             totalNumTextView = rowView.findViewById(R.id.total_num_text);
             bikeTypeTextView = rowView.findViewById(R.id.bike_name_title);
        }
    }

    public CategoryAdapter(List<LinkedHashMap<Integer, Integer>> mDataSet) {
        this.mDataSet = mDataSet;
    }

    private List<LinkedHashMap<Integer,Integer>> mDataSet;


    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.templete_bike_category_image_row,parent,false);
        RowViewHolder viewHolder = new RowViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolder holder, int position) {
        LinkedHashMap<Integer,Integer> item = mDataSet.get(position);

        holder.availableNumTextView.setText( String.valueOf(item.keySet().iterator().next().intValue()));
        holder.totalNumTextView.setText( String.valueOf(item.values().iterator().next().intValue()));
        holder.bikeTypeTextView.setText(Bike.Category.values()[position].name());
    }





    @Override
    public int getItemCount() {
        return mDataSet.size();
    }



    public List<LinkedHashMap<Integer, Integer>> getmDataSet() {
        return mDataSet;
    }

    public void setmDataSet(List<LinkedHashMap<Integer, Integer>> mDataSet) {
        this.mDataSet = mDataSet;
    }
}
