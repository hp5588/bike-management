/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.prepare;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.model.remodel.Category;
import com.hp5588.bikemanagement.fragments.overview.adapters.DetailPrepareListAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPrepareListFragment extends Fragment {

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    Category category;
    public DetailPrepareListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_preparelist, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        DetailPrepareListAdapter detailPrepareListAdapter = new DetailPrepareListAdapter(getContext(), R.layout.template_bike_customer_row,category.getReservations());
        ListView listView = getView().findViewById(R.id.reservation_listview);
        listView.setAdapter(detailPrepareListAdapter);
        detailPrepareListAdapter.notifyDataSetChanged();

    }
}
