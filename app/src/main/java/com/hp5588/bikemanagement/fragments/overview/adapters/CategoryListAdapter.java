/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.fragments.overview.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.model.Bike;
import com.hp5588.bikemanagement.api.model.remodel.Category;

import java.util.List;

public class CategoryListAdapter extends ArrayAdapter<Category> implements View.OnClickListener {


    public CategoryListAdapter(@NonNull Context context, int resource, @NonNull List<Category> objects) {
        super(context, resource, objects);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView==null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.template_bike_category_row,parent,false);
//            convertView = MainActivity.getMainActivity().getLayoutInflater().inflate(R.layout.template_bike_category_row,parent);
        }
        TextView categoryCountTextView = convertView.findViewById(R.id.reservation_category_count);
        TextView categoryNameTextView = convertView.findViewById(R.id.reservation_category_name);
        TextView categoryClientCountTextView = convertView.findViewById(R.id.resercation_category_client_count);

        Category category = getItem(position);

        categoryNameTextView.setText(Bike.Category.values()[position].name());
        if (category!=null) {
            categoryCountTextView.setText(String.valueOf(category.getReservations().size()));
        }else {

            categoryCountTextView.setText("N/A");
        }
//        categoryCountTextView.setText(position);

        return convertView;
    }

    @Override
    public void onClick(View view) {

    }

}
