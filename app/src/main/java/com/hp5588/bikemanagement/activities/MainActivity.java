package com.hp5588.bikemanagement.activities;

import android.Manifest;
import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.hp5588.bikemanagement.R;
import com.hp5588.bikemanagement.api.WebApi;
import com.hp5588.bikemanagement.fragments.bike.BikeFragment;
import com.hp5588.bikemanagement.fragments.bike.ChecklistFragment;
import com.hp5588.bikemanagement.fragments.overview.AvailableFragment;
import com.hp5588.bikemanagement.fragments.overview.OverviewFragment;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.roger.catloadinglibrary.CatLoadingView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements
        BikeFragment.OnFragmentInteractionListener,
        OverviewFragment.OnFragmentInteractionListener,
        AvailableFragment.OnFragmentInteractionListener,
        ChecklistFragment.OnFragmentInteractionListener
{

    private static AppCompatActivity mainActivity;
    private TextView mTextMessage;
    private boolean ready = false;


    List<android.support.v4.app.Fragment> fragments = new ArrayList<>(3);

    private  FragNavController mFragNavController;
    private CatLoadingView loadingView;
    private Toolbar myToolbar;

    private int PERMISSIONS_REQUEST_CAMERA = 1;
    private int PERMISSIONS_REQUEST_INTERNET = 2;
    private int PERMISSIONS_REQUEST_PHONECALL = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = this;
        setContentView(R.layout.activity_main);

        //init API
        WebApi api = WebApi.getInstance();
        api.setUrlString("http://141.70.102.155:3000/");

        loadingView = new CatLoadingView();
        loadingView.setCanceledOnTouchOutside(false);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            requestPermissions(new String[]{Manifest.permission.INTERNET}, PERMISSIONS_REQUEST_INTERNET);
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PERMISSIONS_REQUEST_PHONECALL);
        }

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);


        int[] tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);
        AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                switch (position){
                    case 0:{
                        mFragNavController.switchTab(FragNavController.TAB1);
                        return true;
                    }
                    case 1:{
                        mFragNavController.switchTab(FragNavController.TAB2);
                        return true;
                    }
                    case 2:{
                        mFragNavController.switchTab(FragNavController.TAB3);
                        return true;
                        }
                }
                return false;
            }
        });
        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override
            public void onPositionChange(int y) {

            }
        });
        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.navigation);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation, tabColors);

        FragNavController.Builder builder = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.inner_container);
        fragments.add(new OverviewFragment());
        fragments.add(new BikeFragment());
        fragments.add(new AvailableFragment());
        FragNavTransactionOptions options
                = FragNavTransactionOptions.newBuilder().customAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right).build();

//        builder.defaultTransactionOptions(options).rootFragments(fragments);
        builder.rootFragments(fragments);

        mFragNavController = builder.build();

        ready = true;

    }

    @Override
    public boolean onSupportNavigateUp() {
        popFragment();
        return super.onSupportNavigateUp();
    }

    public static MainActivity getMainActivity() {
        return (MainActivity) mainActivity;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        popFragment();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    @Override
    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

    private void popFragment(){
        int stackSize = mFragNavController.getCurrentStack().size();
        if(stackSize > 1){
            mFragNavController.popFragment();
            if (stackSize == 2 )
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        }
    }



    public FragNavController getmFragNavController() {
        return mFragNavController;
    }


    public CatLoadingView getLoadingView() {
        return loadingView;
    }



    public boolean isReady() {
        return ready;
    }

}
