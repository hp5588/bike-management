/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.utility;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.hp5588.bikemanagement.activities.MainActivity;
import com.roger.catloadinglibrary.CatLoadingView;

public class Helper {

    private static Helper instance;

    private CatLoadingView loadingView;

    public static Helper getInstance() {
        if (instance == null)
            instance = new Helper();
        return instance;
    }

    Helper(){
        loadingView = MainActivity.getMainActivity().getLoadingView();
    }

    public void removeTemplateViewFromLayout(int id, View view){
        ((ViewGroup)view.findViewById(id).getParent()).removeView(view);
    }

    public View loadViewFromLayoutTemplate(int id){
        if (MainActivity.getMainActivity().isReady()){
            LayoutInflater inflater = MainActivity.getMainActivity().getLayoutInflater();
            return inflater.inflate(id,null).findViewById(id);
        }
        return null;
    }

    public void showLoadingView(){
        loadingView.show(MainActivity.getMainActivity().getSupportFragmentManager(),null);
    }

    public void dismissLoadingView(){
        loadingView.dismiss();
    }

    public static void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)  MainActivity.getMainActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(MainActivity.getMainActivity().getCurrentFocus().getWindowToken(), 0);
    }

}
