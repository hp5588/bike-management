/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.hp5588.bikemanagement.utility;

import android.os.AsyncTask;

import com.hp5588.bikemanagement.activities.MainActivity;
import com.roger.catloadinglibrary.CatLoadingView;

public class DefaultAsyncTask extends AsyncTask{
    private CatLoadingView loadingView;
    private android.support.v4.app.FragmentManager fragmentManager;

    public DefaultAsyncTask(){
        loadingView = MainActivity.getMainActivity().getLoadingView();
        fragmentManager = MainActivity.getMainActivity().getSupportFragmentManager();
    }

    @Override
    protected void onPreExecute() {
        loadingView.show(fragmentManager, null);
    }

    @Override
    protected void onPostExecute(Object o) {
        loadingView.dismiss();
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        return null;
    }




}
