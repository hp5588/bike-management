# Introduction
Bike managemant system that help rental service to manage large amount of bikes with ease.

# Features
+ QRCode scanner
+ Return reminder (click to call renter)
+ List bikes to be prepared today
+ Reservation Calendar

# CI
+ Setting up gitlab CI to build artifacts on git push
+ Runner is setup using dockerlized building env
> Docker image: hp5588/android-building-env


# Screenshot

| Overview | Rented Bikes | Search Renter or Bike | Reservation Calendar |
| ------ | ------ | ------ | ------ |
![](images/sc1.png)|![](images/sc2.png)|![](images/sc3.png)|![](images/sc4.png)